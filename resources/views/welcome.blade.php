@extends('base')

@section('title')
    {{env('APP_NAME')}}
@endsection

@section('body')
    <div class="mt-5">
        <div class="h1 text-center">{{env('APP_NAME')}}</div>

        <div class="mt-5">
            <a href="{{route('login')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Login
                With Google</a>
        </div>
    </div>
@endsection
