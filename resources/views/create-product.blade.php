@extends('app')

@section('title')
    Create Product
@endsection

@section('current')
    Create Product
@endsection

@section('content')
    <div class="container">
        <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#info" role="button"
               aria-expanded="false" aria-controls="collapseExample">
                Toggle Info
            </a>
        </p>
        <div class="collapse" id="info">
            <div class="card card-body">
                <div>
                    <div class="h2">Criteria for products.</div>

                    <div class="h5">For products with no Size.</div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">If weight is below 500 gms then dimension should be are L: 8,H:6,W:7
                            (cms)
                        </li>
                        <li class="list-group-item">If weight is between 500 to 1000 gms then dimension should be L:
                            10,H:7,W:12
                            (cms)
                        </li>
                        <li class="list-group-item">If weight is more than 1000 gms then dimension should be L:
                            12,H:11,W:15 (cms)
                        </li>
                    </ul>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item mt-2">
                            <h3 class="mb-2">For Size S.</h3>
                            <h4>Dimensions:</h4>

                            <p class="mb-2">
                                Length - 8cms
                            </p>
                            <p class="mb-2">
                                Height - 6cms
                            </p>
                            <p class="mb-2">
                                Width - 7cms
                            </p>

                            <h4>Weight: 250gms</h4>
                        </li>
                        <li class="list-group-item mt-2">
                            <h3 class="mb-2">For Size M.</h3>
                            <h4>Dimensions:</h4>

                            <p class="mb-2">
                                Length - 9cms
                            </p>
                            <p class="mb-2">
                                Height - 7cms
                            </p>
                            <p class="mb-2">
                                Width - 8cms
                            </p>

                            <h4>Weight: 250gms</h4>
                        </li>
                        <li class="list-group-item mt-2">
                            <h3 class="mb-2">For Size L.</h3>
                            <h4>Dimensions:</h4>

                            <p class="mb-2">
                                Length - 10cms
                            </p>
                            <p class="mb-2">
                                Height - 7cms
                            </p>
                            <p class="mb-2">
                                Width - 12cms
                            </p>

                            <h4>Weight: 250gms</h4>
                        </li>
                        <li class="list-group-item mt-2">
                            <h3 class="mb-2">For Size XL.</h3>
                            <h4>Dimensions:</h4>

                            <p class="mb-2">
                                Length - 12cms
                            </p>
                            <p class="mb-2">
                                Height - 9cms
                            </p>
                            <p class="mb-2">
                                Width - 13cms
                            </p>

                            <h4>Weight: 500gms</h4>
                        </li>
                        <li class="list-group-item mt-2">
                            <h3 class="mb-2">For Size XXL.</h3>
                            <h4>Dimensions:</h4>

                            <p class="mb-2">
                                Length - 12cms
                            </p>
                            <p class="mb-2">
                                Height - 11cms
                            </p>
                            <p class="mb-2">
                                Width - 15cms
                            </p>

                            <h4>Weight: 750gms</h4>
                        </li>
                    </ul>

                </div>
            </div>
        </div>

        <form method="POST" action="{{route('create.product')}}" enctype="multipart/form-data">
            @csrf

            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif

            <div class="form-group">
                <div class="custom-file">
                    <label for="images">Images</label>
                    <input class="form-control-file" type="file" id="images" name="images[]" multiple>
                </div>
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name"
                       value="{{old('name')}}">
            </div>

            <div class="form-group">
                <label for="mrp">MRP</label>
                <input type="number" class="form-control" name="mrp" id="mrp" placeholder="Price of the Product"
                       value="{{old('mrp')}}">
            </div>

            <div class="form-group">
                <label>Size: </label>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="size" id="sizeS"
                           value="S" {{old('size') == 'S' ? 'checked' : ''}}>
                    <label class="form-check-label" for="sizeS">
                        Small
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="size" id="sizeM"
                           value="M" {{old('size') == 'M' ? 'checked' : ''}}>
                    <label class="form-check-label" for="sizeM">
                        Medium
                    </label>
                </div>
                <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="size" id="sizeL"
                           value="L" {{old('size') == 'L' ? 'checked' : ''}}>
                    <label class="form-check-label" for="sizeL">
                        Large
                    </label>
                </div>
                <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="size" id="sizeXL"
                           value="XL" {{old('size') == 'XL' ? 'checked' : ''}}>
                    <label class="form-check-label" for="sizeXL">
                        Extra Large
                    </label>
                </div>
                <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="size" id="sizeXXL"
                           value="XXL" {{old('size') == 'XXL' ? 'checked' : ''}}>
                    <label class="form-check-label" for="sizeXXL">
                        Extra Extra Large
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label for="colour">Colour</label>
                <input type="text" class="form-control" name="colour" id="colour" placeholder="Colour"
                       value="{{old('colour')}}">
            </div>

            <div class="form-group">
                <label for="length">Length</label>
                <input type="number" class="form-control @error('length') is-invalid @enderror" name="length"
                       id="length" placeholder="Length of the Product" value="{{old('length')}}">
            </div>

            <div class="form-group">
                <label for="height">Height</label>
                <input type="number" class="form-control @error('height') is-invalid @enderror" name="height"
                       id="height" placeholder="Height of the Product" value="{{old('height')}}">
            </div>

            <div class="form-group">
                <label for="width">Width</label>
                <input type="number" class="form-control @error('width') is-invalid @enderror" name="width" id="width"
                       placeholder="Width of the Product" value="{{old('width')}}">
            </div>

            <div class="form-group">
                <label for="weight">Weight</label>
                <input type="number" class="form-control @error('weight') is-invalid @enderror" name="weight"
                       id="weight" placeholder="Weight of the Product" value="{{old('weight')}}">
            </div>

            <button type="submit" class="btn btn-primary ">Create</button>
        </form>
    </div>
@endsection

