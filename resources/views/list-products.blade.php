@extends('app')

@section('title')
    List Products
@endsection

@section('current')
    List Products
@endsection

@section('content')
    <div class="container">
        <div class="alert bg-dark text-white" role="alert">
            Hi {{$user->name}} !! Here are your Product listings.
        </div>

        @foreach($products as $product)
            <div class="card text-white bg-dark mb-3">
                <div class="card-header">{{ucwords($product->name)}} - Rs. {{$product->mrp}}</div>
                <div class="card-body">
                    @if(count($product->images) > 0)
                        <div class="container mb-3">
                            <div id="imageCarousel_{{$product->id}}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($product->images as $image)
                                        <div class="carousel-item @if($loop->first) active @endif align-items-center">
                                            <a href="{{$image->image_url}}" target="_blank">
                                                <img class="d-block m-auto"
                                                     width="150px"
                                                     height="150px"
                                                     src="{{$image->image_url}}"
                                                     alt="product image">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#imageCarousel_{{$product->id}}" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#imageCarousel_{{$product->id}}" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    @endif
                    <h5 class="card-title">Colour - {{ucwords($product->colour)}}</h5>
                    <p class="card-text">Length: {{$product->length}} | Height: {{$product->height}} |
                        Width: {{$product->width}} | Weight: {{$product->weight}} </p>
                    @if($product->is_approved)
                        <p class="card-text">Approved</p>
                    @else
                        <p class="card-text">Not Approved</p>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection
