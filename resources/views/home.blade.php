@extends('app')

@section('title')
    Home
@endsection

@section('current')
    Home
@endsection

@section('content')

    <div>
        <div class="h2">Criteria for products.</div>

        <div class="h5">For products with no Size.</div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">If weight is below 500 gms then dimension should be are L: 8,H:6,W:7 (cms)</li>
            <li class="list-group-item">If weight is between 500 to 1000 gms then dimension should be L: 10,H:7,W:12
                (cms)
            </li>
            <li class="list-group-item">If weight is more than 1000 gms then dimension should be L: 12,H:11,W:15 (cms)
            </li>
        </ul>

        <ul class="list-group list-group-flush">
            <li class="list-group-item mt-2">
                <h3 class="mb-2">For Size S.</h3>
                <h4>Dimensions:</h4>

                <p class="mb-2">
                    Length - 8cms
                </p>
                <p class="mb-2">
                    Height - 6cms
                </p>
                <p class="mb-2">
                    Width - 7cms
                </p>

                <h4>Weight: 250gms</h4>
            </li>
            <li class="list-group-item mt-2">
                <h3 class="mb-2">For Size M.</h3>
                <h4>Dimensions:</h4>

                <p class="mb-2">
                    Length - 9cms
                </p>
                <p class="mb-2">
                    Height - 7cms
                </p>
                <p class="mb-2">
                    Width - 8cms
                </p>

                <h4>Weight: 250gms</h4>
            </li>
            <li class="list-group-item mt-2">
                <h3 class="mb-2">For Size L.</h3>
                <h4>Dimensions:</h4>

                <p class="mb-2">
                    Length - 10cms
                </p>
                <p class="mb-2">
                    Height - 7cms
                </p>
                <p class="mb-2">
                    Width - 12cms
                </p>

                <h4>Weight: 250gms</h4>
            </li>
            <li class="list-group-item mt-2">
                <h3 class="mb-2">For Size XL.</h3>
                <h4>Dimensions:</h4>

                <p class="mb-2">
                    Length - 12cms
                </p>
                <p class="mb-2">
                    Height - 9cms
                </p>
                <p class="mb-2">
                    Width - 13cms
                </p>

                <h4>Weight: 500gms</h4>
            </li>
            <li class="list-group-item mt-2">
                <h3 class="mb-2">For Size XXL.</h3>
                <h4>Dimensions:</h4>

                <p class="mb-2">
                    Length - 12cms
                </p>
                <p class="mb-2">
                    Height - 11cms
                </p>
                <p class="mb-2">
                    Width - 15cms
                </p>

                <h4>Weight: 750gms</h4>
            </li>
        </ul>

    </div>
@endsection
