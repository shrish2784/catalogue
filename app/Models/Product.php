<?php

namespace App\Models;

use Database\Factories\ProductFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;


/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property int $mrp
 * @property string|null $size
 * @property string|null $colour
 * @property int $length
 * @property int $height
 * @property int $width
 * @property int $weight
 * @property bool $is_approved
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static ProductFactory factory(...$parameters)
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereColour($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereHeight($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereIsApproved($value)
 * @method static Builder|Product whereLength($value)
 * @method static Builder|Product whereMrp($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereSize($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product whereUserId($value)
 * @method static Builder|Product whereWeight($value)
 * @method static Builder|Product whereWidth($value)
 * @mixin Eloquent
 * @property-read Collection|ProductImage[] $images
 * @property-read int|null $images_count
 */
class Product extends Model
{
    use HasFactory;

    protected $guarded = ['images'];

    public function save(array $options = []): bool {
        $this->is_approved = $this->getApprovalStatus();
        return parent::save($options);
    }

    public function images(): HasMany {
        return $this->hasMany(ProductImage::class);
    }

    private function getApprovalStatus(): bool {
        $is_approved = true;
        if ($this->size == null) {
            if ($this->weight < 500 and ($this->length != 8 or $this->height != 6 or $this->width != 7)) {
                $is_approved = false;
            } elseif ($this->weight >= 500 and $this->weight < 1000 and ($this->length != 10 or $this->height != 7 or $this->width != 12)) {
                $is_approved = false;
            } elseif ($this->weight >= 1000 and ($this->length != 12 or $this->height != 11 or $this->width != 15)) {
                $is_approved = false;
            }
        } elseif ($this->size == 'S' and ($this->length != 8 or $this->height != 6 or $this->width != 7 or $this->weight != 250)) {
            $is_approved = false;
        } elseif ($this->size == 'M' and ($this->length != 9 or $this->height != 7 or $this->width != 8 or $this->weight != 250)) {
            $is_approved = false;
        } elseif ($this->size == 'L' and ($this->length != 10 or $this->height != 7 or $this->width != 12 or $this->weight != 250)) {
            $is_approved = false;
        } elseif ($this->size == 'XL' and ($this->length != 12 or $this->height != 9 or $this->width != 13 or $this->weight != 500)) {
            $is_approved = false;
        } elseif ($this->size == 'XXL' and ($this->length != 12 or $this->height != 11 or $this->width != 15 or $this->weight != 750)) {
            $is_approved = false;
        }

        return $is_approved;
    }
}
