<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductImage
 *
 * @method static Builder|ProductImage newModelQuery()
 * @method static Builder|ProductImage newQuery()
 * @method static Builder|ProductImage query()
 * @mixin Eloquent
 * @property int $id
 * @property int $product_id
 * @property string $image_url
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductImage whereCreatedAt($value)
 * @method static Builder|ProductImage whereId($value)
 * @method static Builder|ProductImage whereImageUrl($value)
 * @method static Builder|ProductImage whereProductId($value)
 * @method static Builder|ProductImage whereUpdatedAt($value)
 */
class ProductImage extends Model
{
    use HasFactory;

    protected $guarded = [];
}
