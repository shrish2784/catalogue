<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public function authorize(): bool {
        return true;
    }

    public function rules(): array {
        return [
            'name'     => 'required|alpha_dash',
            'mrp'      => 'required|integer',
            'size'     => 'nullable|in:S,M,L,XL,XXL',
            'colour'   => 'required',
            'length'   => 'required|integer',
            'height'   => 'required|integer',
            'width'    => 'required|integer',
            'weight'   => 'required|integer',
            'images.*' => 'required|image|mimes:jpeg,png,jpg'
        ];
    }
}
