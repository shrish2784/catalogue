<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;


class GoogleController extends Controller
{

    public function redirectToGoogle(): RedirectResponse {
        return Socialite::driver('google')->redirect();
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    public function handleGoogleCallback() {
        try {
            $user     = Socialite::driver('google')->user();
            $findUser = User::where('google_id', $user->id)->first();

            if ($findUser) {
                Auth::login($findUser);
            } else {
                $newUser = User::create([
                    'name'      => $user->name,
                    'email'     => $user->email,
                    'google_id' => $user->id,
                    'password'  => encrypt('catalogue_pass')
                ]);

                Auth::login($newUser);
            }

            return redirect('/home');
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
