<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Models\Product;
use App\Models\ProductImage;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user     = Auth::user();
        $products = Product::whereUserId($user->id)->get();
        return view('list-products')->with(array('products' => $products, 'user' => $user));
    }

    public function create() {
        return view('create-product');
    }

    /**
     * @throws ValidationException
     */
    public function store(CreateProductRequest $request) {
        $productImages = [];
        $files         = $request->file('images');
        if ($files != null) {
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $name = $file->getClientOriginalName();
                    if (Storage::disk('s3')->put('images/' . $name, file_get_contents($file), 'public')) {
                        $url             = Storage::disk('s3')->url('images/' . $name);
                        $pi              = new ProductImage();
                        $pi->image_url   = $url;
                        $productImages[] = $pi;
                    }
                }
            }
        }

        $user       = Auth::user();
        $validated  = $request->validated();
        $p          = new Product($validated);
        $p->user_id = $user->id;

        try {
            $p->save();
            $p->images()->saveMany($productImages);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw ValidationException::withMessages(['', 'Something Went Wrong.']);
        }
        return redirect(route('home'));
    }
}
