<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    const SIZES = [
        'S', 'M', 'L', 'XL', 'XXL'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('mrp')->default(0);
            $table->enum('size', self::SIZES)->nullable();
            $table->string('colour')->nullable();

            $table->integer('length');
            $table->integer('height');
            $table->integer('width');
            $table->integer('weight');

            $table->boolean('is_approved')->default(false);
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }
}
